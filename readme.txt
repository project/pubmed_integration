
Important:
When importing citations from the PubMed database you assume certain responsibilites. Please make sure that you are familar with the copyright information (http://www.nlm.nih.gov/databases/download.html) and the Entrez user requirements (http://www.ncbi.nlm.nih.gov/entrez/query/static/eutils_help.html#UserSystemRequirements) before you use this module.

As part of implementation of CMS for Biomedical applications, it is often a requirement to display publications from well known databases. One such database is called PubMed which is provided National Center for Biotechnology Information, National Library of Medicine and National Institutes of Health. PubMed includes bibliographic information from MEDLINE, which contains over 17 million citations from life science journals for biomedical articles dating back to the 1950s.

This module integrates with the biblio module (http://drupal.org/project/biblio) to save citations as biblio nodes. PubMed is used as the authoritative source, so if this module locates a previously imported citation it will overwrite any data that is coming in from PubMed. Other fields will remain intact.

Project Sponsors: 
  Joslin Diabetes Center, Harvard Medical School
  Science Collaboration Framework project, http://sciencecollaboration.org/
  Alzheimer Forum Foundation

Project Developers:
  Zaloni (http://www.zaloni.com)
  Agaric Design Collective (http://AgaricDesign.com)
  Acquia inc. (http://acquia.com)
