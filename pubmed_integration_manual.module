<?php

/**
 * @file
 * This module allows a site administrator to 
 * import citations by entering a number of
 * PubMed Ids
 */

/**
 * Implementation of hook_menu().
 */
function pubmed_integration_manual_menu() {
  $items = array();

  $items['admin/settings/pubmed/manual'] = array(
    'title' => t('Import citations by Pubmed Id'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pubmed_integration_manual_import_form'),
    'access arguments' => array('pubmed integration manual import'),  
    'type' => MENU_LOCAL_TASK,
    'weight' => 8,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function pubmed_integration_manual_perm() {
  return array(
    'pubmed integration manual import',
  );
}

/**
 * Allow user to enter PMIDs to import from NCBI
 */
function pubmed_integration_manual_import_form() {
  $form = array();

  $form['pmid_list'] = array(
    '#type' => 'textarea',
    '#title' => t('PubMed ID List'),
    '#description' => t('One or more PubMed ids separated by a comma or one per line.'),
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Import from PubMed')
  );

  return $form;
}

/**
 * Validate PMID entries
 */
function pubmed_integration_manual_import_form_validate($form, &$form_state) {
  //$pmid_list = explode(",", preg_replace("/[^0-9]+/", ",", $form_state['values']['pmid_list']));
  $pmid_list = explode(",", preg_replace("/\n+/", ",", $form_state['values']['pmid_list']));

  foreach($pmid_list as $pmid) {
    if (!empty($pmid) && !is_numeric(trim($pmid))) {
      form_set_error('pmid_list', t('Pubmed Ids must be numeric'));
    }
  }
}

/**
 * Save citations
 */
function pubmed_integration_manual_import_form_submit($form, &$form_state) {
  //$pmid_list = explode(",", preg_replace("/[^0-9]+/", ",", $form_state['values']['pmid_list']));
  $pmid_list = explode(",", preg_replace("/\n+/", ",", $form_state['values']['pmid_list']));

  // Set up the batch import
  $batch = array('operations' => array(),
    'finished' => '',
    'title' => t('Importing from Pubmed'),
    'init_message' => t('Starting the import'),
    'progress_message' => t('Imported @current out of @total'),
    'error_message' => t('An error occurred and some or all of the imports have failed.'),
  );

  // $nids will be the return array of imported nids
  foreach ($pmid_list as $pmid) {
    // Add each item to the batch
    if (!empty($pmid)) {
      $batch['operations'][] = array('pubmed_integration_manual_import', array($pmid, FALSE));
    }
  }

  if (!empty($batch['operations'])) {
    batch_set($batch);
  }
}

/**
 * Helper function to record feedback from the batch functions
 */
function pubmed_integration_manual_import($pmid, $ignore_duplicates = FALSE) {
  $nid = pubmed_integration_import_data($pmid, $ignore_duplicates);

  if (!empty($nid)) {
    $node = node_load($nid[0]);
    drupal_set_message("Successfully Imported: " . $node->biblio_custom3 . " - " . l($node->title, 'node/' . $node->nid));
  }
}
